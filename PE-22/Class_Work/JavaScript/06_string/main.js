const str = "AaaaGKS";

// console.log(getSubstrCount(str, 'a')); // 7
console.log(getSubstrCount(str, 'aa')); // 2

function getSubstrCount(str, substr) {
    let counter = 0;
    // outer: for (let i = 0 ; i < str.length; i++) {
    //     for(let k = 0; k < substr.length; k++) {
    //         if(str[i+k] !== substr[k]) {
    //             // break;
    //             continue outer;
    //         }
    //     }
    //     counter++;
    //     i += substr.length - 1;
    // }
    let index = -1;
    while (true) {
        index = str.indexOf(substr, index + 1);
        if(index !== -1) {
            counter++;
            index += substr.length - 1;
        }
        else {
            break;
        }
    }
    return counter;
}


// console.log(endsWith(str, "012"));
// console.log(endsWith(str, '3'));
//
// function endsWith(str, substr) {
//     return str.lastIndexOf(substr) + substr.length ===  str.length
// }

















