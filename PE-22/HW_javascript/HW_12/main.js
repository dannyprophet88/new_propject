const currentIMG = document.querySelectorAll(".image-to-show");
const buttonStop = document.querySelector(".btn_stop");
const buttonStart = document.querySelector(".btn_start");
const countdown = document.querySelector(".timer");
buttonStart.disabled = true;
let countForIMG = 1;
let timeLeft = 10;
countdown.textContent = timeLeft;
const changerImg = function () {
    countForIMG++;
    currentIMG.forEach( e => {
        if (+e.getAttribute("data-type-title") === countForIMG) {
            e.classList.add("seek");
        } else {
            e.classList.remove("seek");
        }
    });
    if (countForIMG === 4) {
        countForIMG = 0;
    }
};
const tikTok = function () {
    if (timeLeft === 1) {
        timeLeft = 10;
        changerImg();
     } else {
        timeLeft--;
     }
    countdown.textContent = timeLeft;
};
let timeToShow = setInterval(tikTok, 1000);
buttonStop.addEventListener("click", () => {
    clearInterval(timeToShow);
    buttonStart.disabled = false;
});
buttonStart.addEventListener("click", () => {
    timeToShow = setInterval(tikTok, 1000);
    buttonStart.disabled = true;
});