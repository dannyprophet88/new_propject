const arr0 = [3, 2, 4, 5, 10, 232, 533];
console.log(arr0);

// const arr1 = new Array();
// console.log(arr1);
//
// const arr2 = Array(3,4,6,12);
// console.log(arr2);
//
// for (let index = 0; index < arr2.length; index++) {
//     const element = arr2[index];
//     console.log(element);
// }

// const arrayWriter = (element, index) => {
//     console.log(`arr[${index}]: ${element}`);
// };
//
// arr2.forEach(arrayWriter);

// let arr3 = arr2;
// arr3[1] = 0;
// console.log(arr2[1]);
//
// arr2[50] = 1;
// console.log(arr2.length);
// arr2[13] = () => {
//     console.log(`Done!`);
// };
// arr2[13]();
//
// let resultArr = arr0.concat(arr2);
// console.log(resultArr);
//
// let joinArray = arr2.join("-");
// console.log(joinArray);

// arr0.push([1, 2, 4, 2, 5, 6, 0]);
// console.log(arr0);
// arr0.pop(1);
// console.log(arr0);
// arr0.shift(0);
// console.log(arr0);
// arr0.unshift(0);
// console.log(arr0);

// let [num1, num2] = arr0; // деструктуризація
// console.log(`num1: ${num1}, num2: ${num2}`);

resultArr = arr0.slice(1, 5);
console.log(resultArr);

resultArr = arr0.splice(1, 5, 434, 343, 2323, 43, 4);
console.log(resultArr);
console.log(arr0);

const compareFunc = (currentEl, nextEl) => {
    if (currentEl > nextEl) {
        return +1;
    }
    if (currentEl < nextEl) {
        return -1;
    }
    if (currentEl === nextEl) {
        return 0;
    }
};
arr0.sort(compareFunc);
console.log(arr0);

console.log(arr0 instanceof Object);
console.log(arr0);
let result = arr0.indexOf(4);
console.log(result);

const userList = [
    {
        id: 0,
        name: "Net",
        sallary : 15000,
    },
    {
        id: 1,
        name: "Oleho",
        sallary : 10000,
    },
    {
        id: 2,
        name: "Konstantino",
        sallary : 7500,
    },
];

resultArr = userList.map((user) => user.name);
console.log(resultArr);

resultArr = userList.filter((element, index, array) => element.sallary > 1000);
console.log(resultArr);

resultArr = userList.every((element, index, array) => element.sallary > 10000);
console.log(resultArr);

resultArr = userList.some((element, index, array) => element.sallary > 10000);
console.log(resultArr);

result = arr0.reduce((prev, current, index, arr) => {
    return prev + current;
}, 0);
console.log(result);

let storage = [
    'apple',
    'water',
    'banana',
    'pineapple',
    'tea',
    'cheese',
    'coffee',
];
const replaceItems = function (item, ...newItem) {
    let index = storage.indexOf(item);
    storage.splice(index, 1 ,newItem);
};
replaceItems("apples", "eggs", "potato");
console.log(storage);



























