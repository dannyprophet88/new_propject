let Counter = true;

 while(Counter) {
    let numInputOne;
    do {
        numInputOne = prompt('Enter number 1:');
    }while(numInputOne === null || numInputOne.trim() === '' || isNaN(+numInputOne));

    let operation;
    do {
        operation = prompt('Enter operation (+ - * /)');
    }while(operation !== '+' && operation !== '-' && operation !== '*' && operation !== '/');

    let numInputTwo;
    do {
        numInputTwo = prompt('Enter Number 2:');
    }while(numInputTwo === null || numInputTwo.trim() === '' || isNaN(+numInputTwo));

    let result;

    switch (operation) {
        case '+':
            result = +numInputOne + +numInputTwo;
            break;
        case '-':
            result = +numInputOne - +numInputTwo;
            break;
        case '*':
            result = +numInputOne * +numInputTwo;
            break;
        case '/':
            result = +numInputOne / +numInputTwo;
            break;
    }

    alert(result);
    Counter = confirm('One more time?');
}