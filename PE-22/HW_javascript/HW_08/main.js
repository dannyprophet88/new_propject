const input = document.createElement("input");
input.setAttribute("placeholder","Price");
document.body.prepend(input);
const span = document.createElement("span");
const cancelButton = document.createElement("button");
cancelButton.textContent = "x";
input.type = "number";

input.addEventListener('blur',  (evt) => {
    input.style.border = '2px solid green';
    if (input.value <= 0) {
        document.body.append(span);
        span.textContent = 'Please enter correct price';
        span.before(cancelButton);
        input.style.border = '2px solid red';
        input.value = '';
        cancelButton.style.backgroundColor = "red";
        span.style.backgroundColor = "red";
        span.style.color = "white";
        cancelButton.style.color = "white";
    } else {
        document.body.append(span);
        span.textContent = `Текущая цена: ${input.value}`;
        span.before(cancelButton);
        cancelButton.style.backgroundColor = "green";
        span.style.backgroundColor = "green";
        span.style.color = "white";
        cancelButton.style.color = "white";
    }
});

cancelButton.addEventListener("click", function (event) {
    cancelButton.remove();
    span.remove();
    span.textContent = '';
    input.value = '';
});
