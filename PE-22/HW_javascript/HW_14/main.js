$(document).ready(function () {
// ------------Button-to-the-top
    $(window).scroll(function () {
        if ($(this).scrollTop() > 550) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });
    $('.scrollup').click(function () {
        $("html, body").animate({scrollTop: 0}, 2000);
        return false;
    });
// ----------------slide-toggle
    $(".toggle_btn").click(function () {
        $("#hotNews").slideToggle("slow", "swing");
    });
// ----------------NAV-menu
    $("a").on('click', function(event) {
        if (this.hash !== "") {
            event.preventDefault();
            let hash = this.hash;
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 2000, function(){
                window.location.hash = hash;
            });
        }
    });
});