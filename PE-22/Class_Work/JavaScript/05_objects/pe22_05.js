// const flag = true;
// const language = "EN";
//
// const human = {
//     name: "Danny",
//     "last name" : Koen,
//     age: 26,
//     gender: "male",
// };

// const human = {};
//
// human.age = 30;
// console.log(human.age);

// const humanConstructor = function (name, lastName, age, gender) {
//     this.name = name;
//     this.age = age;
//     this.gender = gender;
//     this["last name"] = lastName;
// };
//
// const human1 = new humanConstructor("Uasya", "Pupkin", 30, "male");
//
// console.log(human1);

// const userInfo = {
//     name: "Dan",
//     surName: "Koen",
//     job: "Workless",
//     sayHi: function () {
//         console.log(`Hello, my name is ${this.name} ${this.surName}`);
//     },
//     changeProperty: function (property, value) {
//         if (this[property]) {
//             this[property] = value;
//         } else {
//             console.log("Error");
//         }
//     },
// };
//
// userInfo.changeProperty("name", "Dramij");
// console.log(userInfo.name);

const user = {
    firstName: 'Walter',
    lastName: 'White',
    job: 'Programmer',
    pets: {
        cat: 'Kitty',
        dog: 'Doggy',
    },
};
for (const key in user) {
    console.log(`${key}`);
    if(typeof user [key] === `object`) {
        for (const key2 in user[key]) {
            console.log(`${key2}: ${typeof user[key][key2]} `);
        }
    }else {
        console.log(`${key}: ${typeof user [key]}`);
    }
}



