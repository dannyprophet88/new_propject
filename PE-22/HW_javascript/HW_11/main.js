// const btns = document.getElementsByClassName(".btn");
// console.log(btns);
const buttons = document.querySelectorAll(".btn");
// console.log(buttons);
document.addEventListener("keypress", i => {
    buttons.forEach(e => {
        let currentStyle = getComputedStyle(e);
        if (currentStyle.backgroundColor === 'rgb(0, 0, 255)'){
            e.style.backgroundColor = 'rgb(0, 0, 0)';
        }
        if ( i.key.toUpperCase() === e.textContent.toUpperCase()){
            e.style.backgroundColor = 'rgb(0, 0, 255)';
        }
    });
});

//
// let marker = document.querySelector('#marker');
// let item = document.querySelectorAll('.btn-wrapper .btn');
// function indicator(e) {
//     marker.style.left = e.offsetLeft+ "px";
//     marker.style.width = e.offsetWidth+ "px";
// }
// item.forEach(link => {
//     link.addEventListener("keydown", (e) => {
//         indicator(e.target);
//     })
// });