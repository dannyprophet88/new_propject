// const obj = {};
// obj.name = 'Name';
// obj.getName = function () {
//     return this.name;
// };
// // const obj = {};
// // obj.name = 'Name';                       the same like before!
// // obj.getName = () => this.name;
//
// console.log(obj.getName());

// const user = {
//     name: 'My Name',
//     getNameWithDelay() {
//         console.log(this.name);
//     }
// };
// // setTimeout(user.getNameWithDelay);
// // setTimeout(() => user.getNameWithDelay());
// user.getNameWithDelay();
//
// function mySetTimeout(fn) {
//     fn();
// }

// const user = {
//     name: 'My Name',
//     getNameWithDelay() {
//         setTimeout(() => {
//             console.log(this.name);
//         }, 2000);
//     }
// };
//
// user.getNameWithDelay();
//
// const sum = a => b => a + b;

// function multiply(a, b, c = 1) {
//     return a + b + c;
// }
// function multiply(a,b,c) {
//     if(c === undefined) {
//         c = 1;
//     }
//     return a * b * c;
// }
//
// console.log(multiply(1,2));

//
// const user = {
//     name: 'My Name',
//     data: {
//         age: 10
//     }
// };
//
// // let {data: {age}} = user;
// // age++;
// // console.log(JSON.stringify(user));
//
// // let {name:n, data: {age:a}, prop:p = 123} = user;
// let {property: p = 0, data: {age}} = user;
// // console.log(n, a, p);


// const generateCoord = (x,y) => ({x, y});
// console.log(generateCoord(3, 2)); // {x = 3, y = 2}

//
// const generateCoord = (x, y, z) => (z === undefined ? {x, y} : {x, y, z});
// console.log(generateCoord(3, 2, 1)); // {x = 3, y = 2, z = 1}


const generateCoord = (x, y, z) => {
    if (z === undefined ||(typeof z === 'object' && !z['3d']))
        return {x, y};
    if (typeof z === 'object' && z['3d'])
        z = 0;
    return {x, y, z};
};

// const sum = (a, b) => ({x: a.x + b.x, y: a.y + b.y });
const sum = ({x: ax = 0,y: ay = 0}, {x: bx = 0,y: by = 0}) => ({x: ax + bx, y:ay + by});
const a = generateCoord(3, 2);
const b = generateCoord(4, 1);
// console.log(a);
// console.log(b);
// console.log(sum(a, b));

// console.log(sum({x:3}, {y:4}));
// console.log(sum({y:3}, {x:4}));
// console.log(sum({x:3}, {x:4}));

console.log(generateCoord(3,2));
console.log(generateCoord(3,2), 5);
console.log(generateCoord(3,2),{"3d": true});
console.log(generateCoord(3,2),{"3d": false});


























