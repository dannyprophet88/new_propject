const changeBtn = document.querySelector('.change_btn');
const eventListen = document.head;
if(localStorage.getItem('css' , 'css/alt_style.css') !==null){
    eventListen.querySelector('link[href="css/style.css"]').href = 'css/alt_style.css';
}
changeBtn.addEventListener('click', event =>{
    if(eventListen.querySelector('link[href="css/style.css"]')){
        eventListen.querySelector('link[href="css/style.css"]').href = 'css/alt_style.css';
        localStorage.setItem('css' , 'css/alt_style.css');
    } else {
        eventListen.querySelector('link[href="css/alt_style.css"]').href = 'css/style.css';
        localStorage.clear();
    }
});
