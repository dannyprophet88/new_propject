let massive = [1, 'string', '25', 5, 91, null, 0, {name: 'object'}, 'world', 23, '23', null];

let type = ['number', 'string', 'object', 'undefined'];

let filterBy = (array, dataType) => array.filter(val => typeof(val) !== dataType);

console.log(massive);
console.log(filterBy(massive, type[0]));