// const h1 = document.querySelector("h1");
// const h1_2 = document.querySelectorAll("h1");
// const h1_3 = document.getElementsByTagName("h1");
//
// console.log(h1 === h1_2, h1_2 === h1_3, h1 === h1_3);
//
// const a = document.querySelector(".a");
// const a_2 = document.getElementsByClassName('a')[0];
//
// const id = document.getElementById('b');
// const id_2 = document.querySelector('#b');
// console.log(id, id_2, window.b);

// h1.innerHTML += 'by';
// h1.style.backgroundColor = 'green';
// console.log(h1.style.fontSize);
// const styles = getComputedStyle(h1); // important !
// console.log(styles.fontSize);

// const rows = document.querySelectorAll('tr:nth-child(odd)');
// console.log(rows);
// rows.forEach(row => row.style.backgroundColor = 'green');

// const rows = document.getElementsByTagName('tr');
// Array.from(rows);
// new Array(rows);
// [...rows];
// [].slice.call(rows);  the old one
// [...rows].forEach((row, i) => {
//    if(i % 2 === 0) {
//        row.style.backgroundColor = 'green';
//    }
// });

//
// [...rows]
//     .filter((r,i) => i % 2 === 0)
//     .forEach(row => row.style.backgroundColor = 'red');
//
//

// const obj = {
//     a: 'abc',
//     b: false,
//     c: {},
//     d: 123,
//     e: 0
// };
// // console.log(Object.entries(obj).filter((el) => el[1]));
// Object.entries(obj)
//     .filter(([, val]) => Boolean(val));
//
// console.log(obj);
//
//
// let obj2 = Object.entries(obj)
//     .filter(([, val]) => Boolean(val))  // not complete
//     .reduce((acc , [key]))
//

// const rows = document.querySelectorAll('tr');
// rows.forEach(row => {
//    // row.children
//    //  row.cells
//    //  row.childNodes
//    //  row.querySelectorAll('td, th')
//     const emptyCellsCount = [...row.children]
//         .filter(cell => !cell.innerText.trim())
//         .length;
//     const allEmpty = emptyCellsCount === row.children.length;
//     const allFill = emptyCellsCount === 0;
//     const notEmptyNotFill = !allEmpty && !allFill;
//     if(allEmpty) {
//         row.className += ' all_empty';
//         row.style.backgroundColor = 'red';
//         return;
//     }
//     if(allFill) {
//         row.style.backgroundColor = 'green';
//         return;
//     }
//     // if(notEmptyNotFill) {
//         row.style.backgroundColor = 'blue';
//     // }
// });

const summaryRow = document.querySelector('.summary');
[...summaryRow.cells].forEach((cell, i )=> {
    let sum = 0;
    [...summaryRow.parentElement.children].forEach(tr => {
        sum += +(tr.children[i].innerText);
    });
   cell.innerText = sum;
});





// TREE
// root
// .children
//.parentNode





























