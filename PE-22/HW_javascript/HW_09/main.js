//                    main task
const tabs = document.querySelector('.tabs');
const tabsTitle = document.querySelectorAll('.tabs-title');
const tabsItem = document.querySelectorAll('.tabs-item');
tabs.addEventListener('click', e => {
    const activeItem = document.querySelector(`[data-tab-content ='${e.target.dataset.tabTitle}']`);
    tabsTitle.forEach(element => element.classList.remove('active'));
    e.target.classList.add('active');
    tabsItem.forEach(element => element.classList.remove('active-item'));
    activeItem.classList.add('active-item');
});

//                    marker just for fun
const marker = document.querySelector("#marker");
const item = document.querySelectorAll("ul li");
function indicator(e) {
    marker.style.left = e.offsetLeft+"px";
    marker.style.width = e.offsetWidth+"px";
}
item.forEach(link => {
    link.addEventListener('click', (e)=> {
        indicator(e.target);
    })
});
