const { src, dest, watch } = require('gulp');
const browserSync = require("browser-sync").create();
const sass = require("gulp-sass");
const GulpClient = require('gulp');
const concat = require("gulp-concat");
sass.compiler = require('node-sass');

// gulp.task('default', function(cb) {
//     console.log('gulp run');
//     cb();
// });
const path = {
    src: {
        server: "./src",
        scss: "./src/scss",
        js: "./src/js",
    },
    dest: {
        server: "./dist/",
        css: "./dist/css",
        js: "./dist/js",
    },
};

const mode = "dev";

const serve = function() {
    browserSync.init({
        server: {
            baseDir: path.src.server,
        },
        port: 5500,
        browser: "firefox",
    });
};
const sassFn = function() {
    return src(path.src.scss + "/**/*.scss")
        .pipe(sass({ outputStyle: "compressed" }).on('error', sass.logError))
        .pipe(dest(path[mode].css));
};

const scripts = function() {
    return src(path.src.server + "/**/*.js")
        .pipe(concat("bundle.js"))
        .pipe(dest(path.dest.js));
};

const defaultTask = function() {
    serve();
    sassFn();
    watch(path.src.server + "/**/*.html", function(cb) {
        browserSync.reload();
        cb();
    });
    watch(path.src.scss + "/**/*.scss", function(cb) {
        sassFn();
        browserSync.reload();
        cb();
    });
};

const prod = function() {
    sassFn();
    scripts();
};

exports.default = defaultTask;
// exports.serve = serve;
exports.prodScripts = prod;