let createNewUser = () => {
    let firstName = prompt('Enter your name');
    let surName = prompt('Enter your surname');
    let birthday = prompt('Enter your birth date "dd.mm.yyyy"');
    return {
        firstName: firstName,
        surName: surName,
        login: function () {
            return this.firstName[0].toLowerCase() + this.surName.toLowerCase()
        },
        birthday: birthday,
        getAge: function () {
            const separateNumbers = this.birthday.split('.');
            const userBirthday = new Date(separateNumbers[1] + '.' + separateNumbers[0] + '.' + separateNumbers[2]);
            let userAge = new Date().getFullYear() - userBirthday.getFullYear();
            if (new Date().getMonth() === userBirthday.getMonth() && new Date().getDate() < userBirthday.getDate() || new Date().getMonth() < userBirthday.getMonth()) userAge--;
            return userAge;
        },
        getPassword: function () {
            return this.firstName[0].toUpperCase() + this.surName.toLowerCase() + this.birthday.slice(-4);
        },
    };
};
let newUser = createNewUser();
console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());
